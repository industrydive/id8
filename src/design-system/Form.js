import styled from 'styled-components/macro';
import tw from 'tailwind.macro';

export const Form = styled.form`
  ${tw`bg-white p-8`}
`;

export const InputGroup = styled.div`
  ${tw`mb-4`}
`;

export const Input = styled.input`
  ${tw`shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline mb-2`}
`;

export const Label = styled.label`
  ${tw`block text-gray-700 text-sm font-bold mb-2`}
`;

export const Textarea = styled.textarea`
  ${tw`shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline resize-none`}
`;

export const Error = styled.p`
  ${tw`text-red-500 text-xs italic`}
`;
