import styled from 'styled-components/macro';
import tw from 'tailwind.macro';

const BaseButton = styled.button`
  ${tw`font-bold py-2 px-4 rounded flex-0 mr-1`}
`;

export const PrimaryButton = styled(BaseButton)`
  ${tw`bg-red-500 hover:bg-red-700 text-white`}
`;

export const NeutralButton = styled(BaseButton)`
  ${tw`bg-gray-500 hover:bg-gray-700 text-black`}
`;

export const LinkButton = styled.button`
  ${tw`hover:font-bold`}
`;
