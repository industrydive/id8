import app from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';

const config = {
  apiKey: process.env.REACT_APP_FIREBASE_API_KEY,
  authDomain: process.env.REACT_APP_FIREBASE_AUTH_DOMAIN,
  databaseURL: process.env.REACT_APP_FIREBASE_DATABASE_URL,
  projectId: process.env.REACT_APP_FIREBASE_PROJECT_ID,
  storageBucket: process.env.REACT_APP_FIREBASE_STORAGE_BUCKET,
  messagingSenderId: process.env.REACT_APP_FIREBASE_MESSAGING_SENDER_ID,
  appId: process.env.REACT_APP_FIREBASE_APP_ID,
};

class Firebase {
  constructor() {
    app.initializeApp(config);

    this.auth = app.auth();
    this.db = app.database();

    this.googleProvider = new app.auth.GoogleAuthProvider();
    this.googleProvider.setCustomParameters({
      hd: 'industrydive.com',
    })
  }

  signInWithGoogle = () => {
    return this.auth.signInWithPopup(this.googleProvider);
  }

  signOut = () => {
    return this.auth.signOut();
  }

  onAuthUserListener = (next, fallback) => {
    return this.auth.onAuthStateChanged((authUser) => {
      if (authUser) {
        // if the user is in the database, then merge the db user and firebase auth user
        // if the user is not in the database, then use the fallback method
        this.user(authUser.uid)
          .once('value')
          .then((snapshot) => {
            const dbUser = snapshot.val();

            // default empty roles
            if (!dbUser.roles) {
              dbUser.roles = {};
            }

            // merge the user from Firebase auth and Firebase db
            const user = {
              uid: authUser.uid,
              email: authUser.email,
              ...dbUser,
            };

            next(user);
          })
          .catch(fallback);
      } else {
        fallback();
      }
    });
  }

  user = (uid) => {
    return this.db.ref(`users/${uid}`);
  }

  users = () => {
    return this.db.ref('users');
  }

  idea = (uid) => {
    return this.db.ref(`ideas/${uid}`);
  }

  ideas = () => {
    return this.db.ref(`ideas`);
  }
}

export default Firebase;
