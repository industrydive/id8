import React from 'react';
import styled from 'styled-components/macro';
import tw from 'tailwind.macro';
import { Link } from 'react-router-dom';

import SignOut from '../SignOut';
import { ReactComponent as IdeaIcon } from '../../images/idea.svg';

import * as ROUTES from '../../constants/routes';

const StyledNav = styled.nav`
  ${tw`flex flex-col w-1/12 text-center items-center p-4`}
`;

const StyledLink = styled.li`
  ${tw`pt-4 hover:font-bold`}
`;

const Nav = (props) => {
  return (
    <StyledNav>
      <IdeaIcon width='3rem' />
      <ul>
        <StyledLink>
          <Link to={ROUTES.HOME}>Home</Link>
        </StyledLink>
        <StyledLink>
          <SignOut />
        </StyledLink>
      </ul>
    </StyledNav>
  );
};

export default Nav;
