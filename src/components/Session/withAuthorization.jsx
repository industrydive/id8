import React from 'react';
import { withRouter } from 'react-router-dom';
import { compose } from 'recompose';

import { withFirebase } from '../Firebase';

import * as ROUTES from '../../constants/routes';
import AuthUserContext from './context';

const withAuthorization = condition => Component => {
  class WithAuthorization extends React.Component {
    componentDidMount() {
      this.listener = this.props.firebase.onAuthUserListener(
        (authUser) => {
          if (!condition(authUser)) {
            this.props.history.push(ROUTES.LANDING);
          }
        },
        () => {
          this.props.history.push(ROUTES.LANDING);
        },
      );
    }

    componentWillUnmount() {
      this.listener();
    }

    render() {
      // don't render the component if the condition is false
      return (
        <AuthUserContext.Consumer>
          {(authUser) => {
            return condition(authUser) ? <Component {...this.props} /> : null
          }}
        </AuthUserContext.Consumer>
      )
    }
  }

  return compose(
    withRouter,
    withFirebase,
  )(WithAuthorization);
}

export default withAuthorization;
