import React from 'react';
import { withRouter } from 'react-router-dom';
import { compose } from 'recompose';

import { withFirebase } from '../Firebase';
import SignIn from './SignIn';

import * as ROUTES from '../../constants/routes';

class SignInContainer extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      error: null,
    };
  }

  onSubmit = (event) => {
    const { firebase } = this.props;
    firebase
      .signInWithGoogle()
      .then((socialAuthUser) => {
        if (socialAuthUser.additionalUserInfo.profile.hd === 'industrydive.com') {
          if (socialAuthUser.additionalUserInfo.isNewUser) {
            // create a new user in the db if this is the first time this user has logged in
            return this.props.firebase
              .user(socialAuthUser.user.uid)
              .set({
                username: socialAuthUser.user.displayName,
                email: socialAuthUser.user.email,
                roles: {},
              });
          } else {
            // otherwise, do nothing
            return Promise.resolve();
          }
        } else {
          // if the user is not managed by ID, error out
          return Promise.reject({message: 'Please sign in with an Industry Dive managed Google Account'});
        }
      })
      .then(() => {
        this.setState({
          error: null,
        });
        this.props.history.push(ROUTES.HOME);
      })
      .catch((error) => {
        this.setState({
          error,
        });
      });

    event.preventDefault();
  };

  render() {
    const { error } = this.state;
    const { className } = this.props;

    return (
      <SignIn
        className={className}
        error={error}
        onSubmit={this.onSubmit}
      />
    )
  }
}

const ComposedSignInContainer = compose(
  withRouter,
  withFirebase,
)(SignInContainer);

export default ComposedSignInContainer;
