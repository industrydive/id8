import React from 'react';
import styled from 'styled-components/macro';
import tw from 'tailwind.macro';

import { PrimaryButton } from '../../design-system/Button';

const Form = styled.form`
  ${tw`flex flex-col items-center`};
`;

const SignIn = (props) => {
  const { error, onSubmit, className } = props;

  return (
    <Form className={className} onSubmit={onSubmit}>
      <PrimaryButton type="submit"> Sign in with Google</PrimaryButton>

      {error && <p>{error.message}</p>}
    </Form>
  )
}

export default SignIn;
