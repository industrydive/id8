import React from 'react';
import styled from 'styled-components/macro';
import tw from 'tailwind.macro';

import SignIn from '../SignIn';

const Title = styled.h1`
  ${tw`p-5 text-5xl text-black`};
`;

const LandingPageContainer = styled.section`
  ${tw`flex flex-col flex-auto content-center items-center justify-center w-1/2`};
`;

const StyledSignIn = styled(SignIn)`
  ${tw`p-5`};
`;

const LandingPage = () => {
  return (
    <LandingPageContainer>
      <Title>Share ideas with ID8</Title>
      <StyledSignIn />
    </LandingPageContainer>
  );
};

export default LandingPage;
