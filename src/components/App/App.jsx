import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import styled from 'styled-components/macro';
import tw from 'tailwind.macro';

import HomePage from '../HomePage';
import LandingPage from '../LandingPage';

import { withAuthentication } from '../Session';

import * as ROUTES from '../../constants/routes';

const StyledMain = styled.main`
  ${tw`
    flex flex-row mx-auto
    min-h-screen
    font-sans text-base antialiased font-normal tracking-normal leading-normal
  `}
`;

const App = () => {
  return (
    <Router>
      <StyledMain className="id8-container">
        <Route exact path={ROUTES.LANDING} component={LandingPage} />
        <Route path={ROUTES.HOME} component={HomePage} />
      </StyledMain>
    </Router>
  );
};

export default withAuthentication(App);
