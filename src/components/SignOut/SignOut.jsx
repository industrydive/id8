import React from 'react';

import { LinkButton } from '../../design-system/Button';

import { withFirebase } from '../Firebase';

const SignOut = ({ firebase }) => {
  return (
    <LinkButton onClick={firebase.signOut}>
      Sign Out
    </LinkButton>
  )
}

export default withFirebase(SignOut);
