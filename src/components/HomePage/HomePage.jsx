import React from 'react';
import styled from 'styled-components/macro';
import tw from 'tailwind.macro';

import Nav from '../Nav';
import Ideas from '../Ideas';

import { withAuthorization } from '../Session';


const HomePageContainer = styled.section`
  ${tw`flex flex-col flex-auto content-center items-center justify-start pt-4 w-11/12`};
`;

const HomePage = () => {
  return (
    <>
      <Nav />
      <HomePageContainer>
        <Ideas />
      </HomePageContainer>
    </>
  );
};

const condition = (authUser) => !!authUser;

export default withAuthorization(condition)(HomePage);
