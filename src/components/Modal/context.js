import React from 'react';

const ModalContext = React.createContext({
  show: false,
  showModal: () => {},
  closeModal: () => {},
});

export default ModalContext;
