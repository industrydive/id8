import ModalContext from './context';
import Modal from './Modal';

export { ModalContext, Modal };
