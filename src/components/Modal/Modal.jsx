import React from 'react';
import styled from 'styled-components/macro';
import tw from 'tailwind.macro';

import ModalContext from './context';

import { ReactComponent as CloseIcon } from '../../images/close.svg';

const Overlay = styled.div`
  ${tw`
    fixed inset-0
    z-50
    overflow-auto
    flex
  `}
  background-color: rgb(0, 0, 0, 0.4);
  /* Hide the modal based on show prop */
  ${props => !props.show && tw`hidden`}
`;

const Dialog = styled.div`
  ${tw`
    relative
    bg-white
    w-full max-w-xl
    m-auto
    p-8
    flex flex-col
    rounded
    shadow-md
  `}
`;

const CloseButton = styled.button`
  ${tw`
    absolute top-0 right-0
    p-4
  `}
`;

const Modal = ({ children }) => (
  <ModalContext.Consumer>
    {({ show, closeModal }) => (
      <Overlay show={show}>
        <Dialog>
          {React.cloneElement(children, { closeModal })}
          <CloseButton onClick={closeModal}>
            <CloseIcon css={tw`fill-current text-black hover:text-gray-700`} height="1rem" width="1rem"/>
          </CloseButton>
        </Dialog>
      </Overlay>
    )}
  </ModalContext.Consumer>
);

export default Modal;
