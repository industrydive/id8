import React from 'react';
import { Formik } from 'formik';
import * as Yup from 'yup';
import { Form, InputGroup, Input, Label, Textarea, Error } from '../../design-system/Form';
import { PrimaryButton, NeutralButton } from '../../design-system/Button';

const validationSchema = Yup.object().shape({
  title: Yup.string().required(),
  description: Yup.string().required(),
})

const IdeaForm = ({ idea, onSubmit, deleteIdea, closeModal }) => {

  const submitHandler = (values, actions) => {
    onSubmit(values);
    actions.resetForm();
    closeModal();
  }

  idea && console.log(idea.title);
  const formikProps = {
    initialValues: {
      title: idea ? idea.title : '',
      description: idea ? idea.description : '',
      uid: idea ? idea.uid : null,
    },
    onSubmit: submitHandler,
    validationSchema,
    enableReinitialize: true,
  }

  return (
    <Formik
      {...formikProps}
    >
      {({
        handleSubmit,
        handleChange,
        values,
        errors,
        touched,
        resetForm,
      }) => (
        <Form onSubmit={handleSubmit}>
          <InputGroup>
            <Label>Title</Label>
            <Input
              type="text"
              name="title"
              value={values.title}
              onChange={handleChange}
              placeholder="Give your idea a catchy name"
            />
            {touched.title && errors.title && <Error>{errors.title}</Error>}
          </InputGroup>
          <InputGroup>
            <Label>Description</Label>
            <Textarea
              rows={5}
              name="description"
              value={values.description}
              onChange={handleChange}
              placeholder="Sum up your idea in a few sentences"
            />
            {touched.description && errors.description && <Error>{errors.description}</Error>}
          </InputGroup>
          <PrimaryButton type="submit">Save</PrimaryButton>
          {idea && (
            <NeutralButton type="button"
              onClick={() => {
                resetForm();
                deleteIdea();
                closeModal();
              }}
            >
              Delete
            </NeutralButton>
          )}

          <NeutralButton type="button"
            onClick={() => {
              resetForm();
              closeModal();
            }}
          >
            Cancel
          </NeutralButton>
        </Form>
      )}
    </Formik>
  )

};

export default IdeaForm;
