import React from 'react';

import { AuthUserContext } from '../Session';
import { withFirebase } from '../Firebase';
import { ModalContext, Modal } from '../Modal';

import Ideas from './Ideas';
import IdeaForm from './IdeaForm';

import { PrimaryButton } from '../../design-system/Button';

export const EditingIdeaContext = React.createContext({
  idea: null,
  setEditingIdea: () => {},
});

class IdeasContainer extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      ideas: [],
      isLoading: false,
      isShowingIdeaModal: false,
      editingIdea: null,
    }
  }

  componentDidMount() {
    const { firebase } = this.props;
    this.setState({ isLoading: true });

    firebase.ideas().on('value', snapshot => {
      const ideaObj = snapshot.val();

      if (ideaObj) {
        // convert the structure of the return value into a list
        const ideaList = Object.keys(ideaObj).map(key => ({
          ...ideaObj[key],
          uid: key,
        }));

        this.setState({ ideas: ideaList, isLoading: false });
      } else {
        this.setState({ isLoading: false });
      }
    });
  }

  componentWillUnmount() {
    const { firebase } = this.props;

    firebase.ideas().off();
  };

  submitHandler = (values, authUser) => {
    const { editingIdea } = this.state;
    if (editingIdea && editingIdea.uid === values.uid) {
      this.editIdea(values);
    } else {
      this.createIdea(values, authUser);
    }
  }

  createIdea = ({ title, description }, authUser) => {
    const { firebase } = this.props;
    firebase.ideas().push({
      title,
      description,
      userId: authUser.uid,
    });
  };

  editIdea = ({ uid, title, description }) => {
    const { firebase } = this.props;
    const { editingIdea } = this.state;
    firebase.idea(uid).set({
      ...editingIdea,
      title,
      description,
    });
  };

  deleteIdea = () => {
    const { firebase } = this.props;
    const { editingIdea } = this.state;
    if (editingIdea) {
      firebase.idea(editingIdea.uid).remove();
    }
    this.setState({ editingIdea: null });
  }

  showIdeaModal = () => {
    this.setState({ isShowingIdeaModal: true });
  };

  closeIdeaModal = () => {
    this.setState({ isShowingIdeaModal: false, editingIdea: null });
  };

  setEditingIdea = (editingIdea) => {
    this.setState({ editingIdea });
  };

  render() {
    const { ideas, isLoading, isShowingIdeaModal, editingIdea } = this.state;

    return (
      <AuthUserContext.Consumer>
        {(authUser) => (
          <ModalContext.Provider
            value={{
              show: isShowingIdeaModal,
              closeModal: this.closeIdeaModal,
              showModal: this.showIdeaModal,
            }}
          >
            <PrimaryButton onClick={this.showIdeaModal} >Add Idea</PrimaryButton>
            <EditingIdeaContext.Provider value={{ editingIdea, setEditingIdea: this.setEditingIdea }}>
              <Ideas
                ideas={ideas}
                isLoading={isLoading}
              />
              <Modal>
                <IdeaForm
                  idea={editingIdea}
                  onSubmit={values => this.submitHandler(values, authUser)}
                  deleteIdea={this.deleteIdea}
                />
              </Modal>
            </EditingIdeaContext.Provider>
          </ModalContext.Provider>
        )}
      </AuthUserContext.Consumer>
    );
  }
}

const ComposedIdeasContainer = withFirebase(IdeasContainer);

export default ComposedIdeasContainer;
