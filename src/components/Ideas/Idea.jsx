import React from 'react';
import styled from 'styled-components/macro';
import tw from 'tailwind.macro';

import { EditingIdeaContext } from './IdeasContainer';
import { ModalContext } from '../Modal';

const StyledArticle = styled.article`
  ${tw`
    p-4
    my-4 mx-12
    text-center
    border-2 border-solid border-gray-700 hover:border-red-700 rounded
    w-1/4
  `}
`;

const Title = styled.h2`
  ${tw`
    pb-1
    text-black
    font-semibold
    text-2xl
  `}
`;

const Description = styled.p`
  ${tw`
    pb-2
    text-black text-left
    whitespace-pre-wrap
  `}
`;

const User = styled.p`
  ${tw`
    pb-1
    text-gray-700 text-xs
  `}
`;

const Idea = ({idea}) => {
  return (
    <EditingIdeaContext.Consumer>
      {({ setEditingIdea }) => (
        <ModalContext.Consumer>
          {({ showModal }) => (
            <StyledArticle onClick={() => {
              setEditingIdea(idea);
              showModal();
            }}>
              <header>
                <Title>{idea.title}</Title>
                <User>By: {idea.userId}</User>
              </header>
              <Description>{idea.description}</Description>
            </StyledArticle>
          )}
        </ModalContext.Consumer>
      )}
    </EditingIdeaContext.Consumer>
  );
};

export default Idea;
