import React from 'react';
import styled from 'styled-components/macro';
import tw from 'tailwind.macro';

import Idea from './Idea';

const IdeasContainer = styled.section`
  ${tw`
    flex flex-row justify-between flex-wrap
    w-full
    p-4
  `}
`;


const Ideas = ({ ideas, isLoading }) => (
  <IdeasContainer>
    {ideas.map(idea => (
      <Idea
        key={idea.uid}
        idea={idea}
      />
    ))}
  </IdeasContainer>
);

export default Ideas;
