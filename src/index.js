import React from 'react';
import ReactDOM from 'react-dom';
import { ThemeProvider, createGlobalStyle } from 'styled-components/macro';
import preflight from 'styled-preflight';

import App from './components/App';
import Firebase, { FirebaseContext } from './components/Firebase';

import theme from './theme';
import * as serviceWorker from './serviceWorker';

// Include Preflight (from tailwind) globally before we start using Styled Components
const GlobalStyle = createGlobalStyle`
  ${preflight}
`;

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <FirebaseContext.Provider value={new Firebase()}>
      <GlobalStyle />
      <App />
    </FirebaseContext.Provider>
  </ThemeProvider>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
