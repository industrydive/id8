# Notes for blog post

- We live! https://id8-app.web.app/

- `create-react-app`
  - Pros
    - Nice to not have to do tooling
  - Cons
    - Adding packages into tooling is hard
      - Tailwind was easy to set up thanks to prior discovery
    - Default linting is not Airbnb
      - Can't configure since it's hidden behind `create-react-app` config
- Tailwind + Babel macros
  - Honestly, a huge pain in the ass
  - Had to use `styled-preflight` to include the normalize CSS (`preflight`) that Tailwind **usually** includes, but that we miss out on
    since we're using the Babel macros
  - https://wetainment.com/articles/tailwind-css-in-js/
  - https://blog.logrocket.com/create-react-app-and-tailwindcss/
  - Watch out for extra semi-colon! Will break things that come after it
- Container/Presentational components model
  - https://scotch.io/courses/5-essential-react-concepts-to-know-before-learning-redux/presentational-and-container-component-pattern-in-react
  - https://medium.com/@dan_abramov/smart-and-dumb-components-7ca2f9a7c7d0
- React + Accessibility
  - https://alligator.io/react/making-react-apps-more-accessible/
- Firebase security rules
  - https://howtofirebase.com/firebase-security-rules-88d94606ce4a
- You can't `yarn build` a development build
  - https://create-react-app.dev/docs/adding-custom-environment-variables
